import { Component, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { fromEvent } from 'rxjs';
import { GlobalAnimation } from 'src/app/Animations/GlobalAnimation';
import { MenuItems } from 'src/app/Service/staticData';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  animations: [
    GlobalAnimation.scaleY('8px', { backgroundColor: '#FA0A00', opacity: 1, animationSpeed: '0.2s' }),
    GlobalAnimation.scaleY('12vh', { altName: 'scalHeader', backgroundColor: '#000000' })
  ]
})
export class HeaderComponent implements OnInit {
  MenuList = MenuItems
  hoveredMenu: boolean[] = []
  menuKeys: string[] = []
  headerHidden = false;
  headerShow = false;

  constructor(
    public router: Router
  ) {
    this.menuKeys = Object.keys(this.MenuList).map((k) => k);
    this.hoveredMenu = this.menuKeys.map(() => false);
  }

  ngOnInit(): void {

    const observer = new IntersectionObserver((entries) => {
      if (entries[0].isIntersecting === true) {
        console.log("Element is fully visible in screen");
        this.headerHidden = false;
      }
      else if (entries[0].isIntersecting === false) {
        console.log("Element is fully invisible in screen")
        this.headerHidden = true;
      }
    }, { threshold: [0] })

    const observer2 = new IntersectionObserver((entries) => {
      if (entries[0].isIntersecting === true) {
        // console.log("Element is fully visible in screen");
        this.headerShow = false;
      }
      else if (entries[0].isIntersecting === false) {
        // console.log("Element is fully invisible in screen")
        this.headerShow = true;
      }
    }, { threshold: [1] })

    const element = document.getElementById('header-root-element-selector-id');

    if (element) {
      observer.observe(element);
      observer2.observe(element);
    }

  }

  checkRout(route: string, rvalue: string) {
    // console.log(route, rvalue)
    return route === '/' + rvalue
  }

  changeMenu(id: number, state: boolean) {
    this.hoveredMenu[id] = state;
  }

  changeRoute(path: string) {
    this.router.navigate([path]);
  }

}
