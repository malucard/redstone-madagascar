import { Component, OnInit } from '@angular/core';
import { GlobalAnimation } from 'src/app/Animations/GlobalAnimation';
@Component({
  selector: 'app-galerie',
  templateUrl: './galerie.component.html',
  styleUrls: ['./galerie.component.css'],
  animations: [GlobalAnimation.scaleY('300px', { backgroundColor: '#c90000' }), GlobalAnimation.opacity()]
})
export class GalerieComponent implements OnInit {
  baseImageUrl = "https://via.placeholder.com/500x300";
  constCardData: any[] = []
  showDetails: boolean[] = []

  constructor() {
    this.constCardData = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].map((c) => ({
      imageUrl: this.baseImageUrl,
      title: "Card title " + c,
      subtile: "Subtitle " + c,
      detail: "Card details " + c
    }));
    this.showDetails = this.constCardData.map((c) => false)
  }

  setImageStyle(imageUrl: string) {
    return "background-image: url('$');".replace('$', imageUrl);
  }

  setShowDetails(id: number, state: boolean) {
    this.showDetails[id] = state
  }

  ngOnInit(): void {
  }

}
