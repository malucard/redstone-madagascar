import { ContactsComponent } from './Components/page/contacts/contacts.component';
import { ServicesComponent } from './Components/page/services/services.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './Components/home/home.component';
import { GalerieComponent } from './Components/page/galerie/galerie.component';
import { GLOBALROUTS } from './Service/staticData';

const routes: Routes = [
  { path: GLOBALROUTS.home, component: HomeComponent },
  { path: GLOBALROUTS.galerie, component: GalerieComponent },
  { path: GLOBALROUTS.services, component: ServicesComponent },
  { path: GLOBALROUTS.constact, component: ContactsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
