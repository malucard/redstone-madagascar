export const GLOBALROUTS = {
  home: 'home',
  galerie: 'realisation',
  services: 'services',
  constact: 'constact'
}

export const MenuItems: any = {
  home: {
    id: 0,
    title:'Accueil',
    path: GLOBALROUTS.home
  },
  galerie:{
    id: 1,
    title:'Nos Réalisations',
    path: GLOBALROUTS.galerie
  },
  servisce:{
    id: 2,
    title:'Nos Services',
    path: GLOBALROUTS.services
  },
  contact:{
    id: 3,
    title:'Contactez-nous',
    path: GLOBALROUTS.constact
  }
}
