import { animate, state, style, transition, trigger } from "@angular/animations";

export const GlobalAnimation = {
  scaleY: (Maxheight: string, params?: {
    altName?: string,
    backgroundColor?: string,
    opacity?: number,
    animationSpeed?: string,
    aditionalStyle?: any }) => trigger(params?.altName ?? 'scaleY', [
    state('open', style({
      height: Maxheight,
      opacity: params?.opacity ?? 0.8,
      backgroundColor: params?.backgroundColor ?? 'yellow',
      ...params?.aditionalStyle
    })),
    state('closed', style({
      height: 0,
      opacity: params?.opacity ?? 0,
      backgroundColor: params?.backgroundColor ?? '#c6ecff',
      ...params?.aditionalStyle
    })),
    transition('open => closed', [
      animate(params?.animationSpeed ?? '0.5s')
    ]),
    transition('closed => open', [
      animate(params?.animationSpeed ?? '0.5s')
    ])
  ]),
  opacity: (animationSpeed?: string) => trigger('opacity', [
    state('open', style({
      opacity: 1,
    })),
    state('closed', style({
      opacity: 0,
    })),
    transition('open => closed', [
      animate(animationSpeed ?? '0.7s')
    ]),
    transition('closed => open', [
      animate(animationSpeed ?? '1s')
    ])
  ])
}
